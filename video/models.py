import os

from django.core.exceptions import ValidationError
from django.db import models

from channel.models import Channel
from home.models import User


def generate_token():
    import binascii
    return binascii.hexlify(os.urandom(20)).decode()


def validate_video_file(value):
    if value.content_type != 'video/mp4':
        raise ValidationError('Please upload a valid video file in .mp4 format.')


class Video(models.Model):
    title = models.CharField(max_length=128)
    description = models.TextField(null=True, blank=True)
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE, related_name='videos')
    file = models.FileField(upload_to='videos/%Y/%m/%d/', validators=[validate_video_file])
    # TODO: disable direct public access to videos
    uploaded_at = models.DateTimeField(auto_now_add=True)
    stream_price = models.DecimalField(verbose_name="Streaming Price", max_digits=10, decimal_places=2, default=0)
    purchase_price = models.DecimalField(verbose_name="Streaming Price", max_digits=10, decimal_places=2, default=0)

    def __str__(self):
        return self.title


class VideoStreamToken(models.Model):
    video = models.ForeignKey(Video, on_delete=models.CASCADE, related_name='stream_tokens')
    token = models.CharField(max_length=40, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

    @staticmethod
    def create_and_get_object(video_id):
        return VideoStreamToken.objects.create(video_id=video_id, token=generate_token())

    def __str__(self):
        return self.token


class VideoSubscription(models.Model):
    video = models.ForeignKey(Video, on_delete=models.CASCADE, related_name='subscriptions')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='video_subscriptions')
    rented_at = models.DateTimeField(null=True, blank=True)
    purchased_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    streamed_at = models.DateTimeField(verbose_name='Streamed at (first time)', null=True, blank=True)
    watch_till = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.pk)

    class Meta:
        unique_together = ('video', 'user',)


class VideoTransaction(models.Model):
    PAYMENT_STATUSES = (
        (1, 'Successful'),
        (2, 'Failed'),
    )

    subscription = models.ForeignKey(VideoSubscription, on_delete=models.CASCADE, related_name='transactions')
    paid_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='video_transactions_paid')
    charge_id = models.CharField(max_length=64)
    transaction_id = models.CharField(max_length=64)
    status = models.SmallIntegerField(choices=PAYMENT_STATUSES)
    amount = models.IntegerField()
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.charge_id
