from django.contrib import admin

from video.models import Video, VideoStreamToken, VideoSubscription, VideoTransaction


class VideoAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'channel', 'file', 'uploaded_at', 'stream_price', 'purchase_price',)


admin.site.register(Video, VideoAdmin)


class VideoStreamTokenAdmin(admin.ModelAdmin):
    list_display = ('video', 'token', 'created_at',)


admin.site.register(VideoStreamToken, VideoStreamTokenAdmin)


class VideoSubscriptionAdmin(admin.ModelAdmin):
    list_display = ('video', 'user', 'rented_at', 'purchased_at', 'created_at', 'streamed_at', 'watch_till',)


admin.site.register(VideoSubscription, VideoSubscriptionAdmin)


class VideoTransactionAdmin(admin.ModelAdmin):
    list_display = ('subscription', 'paid_by', 'charge_id', 'transaction_id', 'status', 'amount', 'description',
                    'created_at',)


admin.site.register(VideoTransaction, VideoTransactionAdmin)
