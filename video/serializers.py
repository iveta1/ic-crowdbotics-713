from rest_framework import serializers

from .models import Video


class VideoSerializer(serializers.ModelSerializer):
    is_rented = serializers.SerializerMethodField(read_only=True)
    is_purchased = serializers.SerializerMethodField(read_only=True)
    net_purchase_price = serializers.SerializerMethodField(read_only=True)

    def get_net_purchase_price(self, obj):
        is_rented = self.get_is_rented(obj)
        is_purchased = self.get_is_purchased(obj)
        if is_rented and not is_purchased:
            return obj.purchase_price - obj.stream_price
        elif is_rented and is_purchased:
            return 0
        else:
            return obj.purchase_price

    def _get_sub_qs(self, obj):
        user = self.context['request'].user if 'request' in self.context else None
        if user:
            return user.video_subscriptions.filter(video=obj)
        return None

    def get_is_rented(self, obj):
        qs = self._get_sub_qs(obj)
        if qs:
            return qs.filter(rented_at__isnull=False).exists()
        return False

    def get_is_purchased(self, obj):
        qs = self._get_sub_qs(obj)
        if qs:
            return qs.filter(purchased_at__isnull=False).exists()
        return False

    def validate(self, attrs):
        user = self.context['request'].user
        channel = attrs['channel']

        if channel.owner != user and not channel.publishers.filter(pk=user.pk).exists():
            raise serializers.ValidationError('You do not have the required privileges.')

        return attrs

    class Meta:
        model = Video
        fields = ('id', 'title', 'description', 'channel', 'file', 'uploaded_at', 'stream_price', 'purchase_price',
                  'net_purchase_price', 'is_rented', 'is_purchased',)
        extra_kwargs = {
            'file': {
                'write_only': True,
            },
            'channel': {
                'write_only': True,
            },
        }
