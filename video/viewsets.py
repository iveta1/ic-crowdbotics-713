import mimetypes
import os
import re
from wsgiref.util import FileWrapper

import stripe
from django.db import transaction
from django.http import StreamingHttpResponse
from django.utils import timezone
from rest_framework import viewsets, mixins, pagination, parsers, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from ic_crowdbotics_713 import settings
from video.file_wrappers import RangeFileWrapper
from video.models import Video, VideoSubscription, VideoTransaction, VideoStreamToken
from video.serializers import VideoSerializer


class VideoViewSet(mixins.CreateModelMixin,
                   # mixins.RetrieveModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    serializer_class = VideoSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = pagination.PageNumberPagination
    pagination_class.page_size = 10
    parser_classes = (parsers.FormParser, parsers.MultiPartParser,)

    def get_queryset(self):
        queryset = Video.objects.order_by('-uploaded_at')
        if self.action == 'list':
            channel_id = self.request.GET.get('channel_id', self.request.user.channels.first().pk)
            queryset = queryset.filter(channel_id=channel_id)
        # elif self.action in ['get_token', 'watch']:
        #     queryset = queryset.filter(subscriptions__user=self.request.user) \
        #         .filter(Q(subscriptions__purchased_at__isnull=False) | Q(subscriptions__watch_till__gte=timezone.now()))
        return queryset

    def _is_user_controls_channel(self, user_id, channel):
        return channel.owner.pk == user_id or channel.moderators.filter(pk=user_id).exists() \
               or channel.publishers.filter(pk=user_id).exists() \
               or channel.editors.filter(pk=user_id).exists()

    def _is_user_allowed_to_stream(self, user_id, video_id):
        video = Video.objects.get(pk=video_id)
        if self._is_user_controls_channel(user_id=user_id, channel=video.channel):
            return True

        subscription = VideoSubscription.objects.get(video_id=video_id, user_id=user_id)
        if subscription.user.pk == user_id and (
                subscription.purchased_at is not None or subscription.watch_till > timezone.now()):
            return True
        return False

    @action(detail=True, methods=['get'], permission_classes=[IsAuthenticated], parser_classes=[parsers.JSONParser])
    def get_token(self, request, pk=None):
        if not self._is_user_allowed_to_stream(user_id=request.user.pk, video_id=pk):
            self.permission_denied(request, 'You do not have access to this resource')

        video_stream_token = VideoStreamToken.create_and_get_object(video_id=pk)
        return Response(data={'token': video_stream_token.token})

    @action(detail=False, methods=['get'], permission_classes=[IsAuthenticated], parser_classes=[parsers.JSONParser],
            pagination_class=None)
    def watch(self, request):
        """
        `v`: stream token
        """
        token = request.GET.get('v')
        video_stream_obj = VideoStreamToken.objects.get(token=token)
        object = video_stream_obj.video
        video_path = object.file.path

        if not self._is_user_allowed_to_stream(user_id=request.user.pk, video_id=object.pk):
            self.permission_denied(request, 'You do not have access to this resource')

        range_re = re.compile(r'bytes\s*=\s*(\d+)\s*-\s*(\d*)', re.I)

        range_header = request.META.get('HTTP_RANGE', '').strip()
        range_match = range_re.match(range_header)
        size = os.path.getsize(video_path)
        content_type, encoding = mimetypes.guess_type(video_path)
        content_type = content_type or 'application/octet-stream'
        if range_match:  # called while video buffers and continuously loads
            first_byte, last_byte = range_match.groups()
            first_byte = int(first_byte) if first_byte else 0
            last_byte = int(last_byte) if last_byte else size - 1
            if last_byte >= size:
                last_byte = size - 1
            length = last_byte - first_byte + 1
            resp = StreamingHttpResponse(RangeFileWrapper(open(video_path, 'rb'), offset=first_byte, length=length),
                                         status=206, content_type=content_type)
            resp['Content-Length'] = str(length)
            resp['Content-Range'] = 'bytes %s-%s/%s' % (first_byte, last_byte, size)
        else:  # called when video first loaded
            if video_stream_obj.created_at < timezone.now() - timezone.timedelta(seconds=15):
                video_stream_obj.delete()
                self.permission_denied(request, 'Invalid request')
            VideoStreamToken.objects.filter(created_at__lte=timezone.now() - timezone.timedelta(seconds=60)).delete()
            resp = StreamingHttpResponse(FileWrapper(open(video_path, 'rb')), content_type=content_type)
            resp['Content-Length'] = str(size)
        resp['Accept-Ranges'] = 'bytes'

        if not self._is_user_controls_channel(user_id=request.user.pk, channel=object.channel):
            subscription = object.subscriptions.filter(user=request.user).get()
            if subscription.streamed_at is None:
                subscription.streamed_at = timezone.now()
                if subscription.purchased_at is None:
                    subscription.watch_till = timezone.now() + timezone.timedelta(hours=12)
                subscription.save()
        return resp

    def list(self, request, *args, **kwargs):
        """
        `channel_id`: Get videos of this channel. Default value: user's default channel
        """
        return super().list(request, *args, **kwargs)

    def _get_stripe_amount(self, video, is_rent, request):
        if self._is_user_controls_channel(user_id=request.user.pk, channel=video.channel):
            self.permission_denied(request, 'Invalid request')

        sub_qs = VideoSubscription.objects.filter(video=video, user=request.user)
        sub = None
        if sub_qs.exists():
            sub = sub_qs.get()
            if sub.purchased_at is not None:
                self.permission_denied(request, 'You have already purchased this video')
            if is_rent and sub.rented_at is not None:
                self.permission_denied(request, 'You have already rented this video')

        if is_rent:
            amount = video.stream_price
        elif sub is not None and sub.rented_at is not None:
            amount = video.purchase_price - video.stream_price
        else:
            amount = video.purchase_price

        return int(amount * 100)

    @action(detail=True, methods=['get'], permission_classes=[IsAuthenticated], parser_classes=[parsers.JSONParser])
    def init_transaction(self, request, pk=None):
        """
        `is_rent`: Boolean field. Default value: False.
        """
        is_rent = request.GET.get('is_rent', False)

        video = Video.objects.get(pk=pk)
        amount = self._get_stripe_amount(video=video, is_rent=is_rent, request=request)

        data = {
            'stripe_key': settings.STRIPE_PUBLIC_KEY,
            'amount': amount,
            'name': '{} Video'.format('Rent' if is_rent else 'Purchase'),
            'description': video.title,
            # 'image': None,
            'email': request.user.email
        }

        return Response(data=data, status=status.HTTP_200_OK)

    @transaction.atomic
    @action(detail=True, methods=['post'], permission_classes=[IsAuthenticated], parser_classes=[parsers.JSONParser])
    def capture_transaction(self, request, pk=None):
        """
        `is_rent`: Boolean field. Default value: False.
        `stripe_token`: (Required) String
        `stripe_email`: (Required) Email
        """
        try:
            is_rent = request.data.get('is_rent', False)
            stripe_token = request.data.get('stripe_token', None)
            stripe_email = request.data.get('stripe_email', None)

            video = Video.objects.get(pk=pk)
            amount = self._get_stripe_amount(video=video, is_rent=is_rent, request=request)

            charge = stripe.Charge.create(
                description=video.title,
                amount=amount,
                currency='usd',
                source=stripe_token,
                receipt_email=stripe_email,
                api_key=settings.STRIPE_SECRET_KEY
            )

            payment_successful = charge.paid and charge.captured

            sub_qs = VideoSubscription.objects.filter(video=video, user=request.user)
            if sub_qs.exists():
                sub = sub_qs.get()
            else:
                sub = VideoSubscription(video=video, user=request.user)

            if is_rent:
                sub.rented_at = timezone.now()
                sub.watch_till = timezone.now() + timezone.timedelta(hours=48)
            else:
                sub.purchased_at = timezone.now()
                sub.watch_till = None
            sub.save()

            VideoTransaction.objects.create(
                subscription=sub,
                paid_by=request.user,
                charge_id=charge.id,
                transaction_id=charge.balance_transaction,
                status=1 if payment_successful else 2,
                amount=amount,
                description='{} reason: {}'.format(
                    charge.status, charge.failure_message
                ) if not payment_successful else None,
            )

            return Response(status=status.HTTP_204_NO_CONTENT)
        except stripe.error.CardError as e:
            # Since it's a decline, stripe.error.CardError will be caught
            exception = e
        except stripe.error.RateLimitError as e:
            # Too many requests made to the API too quickly
            exception = e
        except stripe.error.InvalidRequestError as e:
            # Invalid parameters were supplied to Stripe's API
            exception = e
        except stripe.error.AuthenticationError as e:
            # Authentication with Stripe's API failed
            # (maybe you changed API keys recently)
            exception = e
        except stripe.error.APIConnectionError as e:
            # Network communication with Stripe failed
            exception = e
        except stripe.error.StripeError as e:
            # Display a very generic error to the user, and maybe send
            # yourself an email
            exception = e
        except Exception as e:
            # Something else happened, completely unrelated to Stripe
            exception = e
        if settings.DEBUG:
            print(exception)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
