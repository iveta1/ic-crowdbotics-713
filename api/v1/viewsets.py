from django.contrib.auth import get_user_model

from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.viewsets import ModelViewSet, ViewSet
from rest_framework.views import APIView
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status

from friendship.models import Friend, Follow, Block, FriendshipRequest
from api.v1.permissions import IsCurrentUserOrIsStaff
from api.v1.serializers import SignupSerializer, UserSerializer, FriendshipRequestSerializer, UserProfileSerializer


User = get_user_model()

class SignupViewSet(ModelViewSet):
    serializer_class = SignupSerializer
    http_method_names = ['post']


class UserProfileViewSet(ModelViewSet):
    serializer_class = UserProfileSerializer
    model = User
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated, IsCurrentUserOrIsStaff,)

    @action(detail=False, methods=['post'])
    def submit_for_verification(request):
        """

        """
        done, msg = user.submit_for_verification()
        if done:
            return Response({'details':msg}, status=status.HTTP_200_OK)
        return Response({'details':msg}, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        return Response({"detail": "Method not allowed!"})

class LoginViewSet(ViewSet):
    serializer_class = AuthTokenSerializer

    def create(self, request):
        return ObtainAuthToken().post(request)


class FriendshipViewSet(ViewSet):
    model = User
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    @action(detail=False, methods=['post'])
    def get_friends(self, request, pk=None):
        """
            `user_id` : (optional)This is the user id of the user for which
                friends are to be obtained. If user_id is not provided, the logged
                in user is considered by default.
        """
        pk = request.data.get('user_id')

        if pk:
            self.object = self.model.objects.get(pk=pk)
        else:
            self.object = request.user

        friends = Friend.objects.friends(self.object)
        serializer = self.serializer_class(friends, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(detail=False, methods=['post'])
    def send_request(self, request, pk=None):
        """
            `user_id`: User id of the user to whom the request has
                to be sent
        """
        pk = request.data.get('user_id')
        if pk:
            msg = request.data.get('message')
            other_user = User.objects.get(pk=pk)
            Friend.objects.add_friend(
                request.user,
                other_user,
                message=msg if msg else None)
            return Response({"detail":"Request Sent"}, status=status.HTTP_200_OK)
        return Response({"detail":"User ID not provided"}, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def get_all_requests(self, request, pk=None):
        """
            Displays all the requests for the current logged in user
        """
        self.object = request.user
        requests = Friend.objects.unrejected_requests(self.object)
        serializer = FriendshipRequestSerializer(requests, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(detail=False, methods=['post'])
    def get_rejected_requests(self, request, pk=None):
        """
            Displays all the requests which have been rejected by the logged in user
        """
        self.object = request.user
        requests = Friend.objects.rejected_requests(self.object)
        serializer = FriendshipRequestSerializer(requests, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(detail=False, methods=['post'])
    def get_request_count(self, request, pk=None):
        """
            Returns the request count
        """
        self.object = request.user
        count = Friend.objects.unrejected_request_count(self.object)
        return Response({"count":count}, status=status.HTTP_200_OK)

    @action(detail=False, methods=['post'])
    def get_sent_requests(self, request, pk=None):
        """
            Returns all the requests sent out by the logged in user
        """
        self.object = request.user
        requests = Friend.objects.sent_requests(self.object)
        serializer = FriendshipRequestSerializer(requests, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(detail=False, methods=['post'])
    def accept_request(self, request, pk=None):
        """
            Accepts the request between 2 users through request ID.

            `request_id`: ID of the request between the users. (Obtained from
                request list)
        """
        pk = request.data.get('request_id')
        if pk:
            self.object = request.user
            request = FriendshipRequest.objects.get(pk=pk)
            request.accept()
            return Response({"detail":"Request accepted"}, status=status.HTTP_200_OK)
        return Response({"detail":"Request ID not provided"}, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def reject_request(self, request, pk=None):
        """
            Rejects the request between 2 users through request ID.

            `request_id`: ID of the request between the users. (Obtained from
                request list)
        """
        pk = request.data.get('request_id')
        if pk:
            self.object = request.user
            request = FriendshipRequest.objects.get(pk=pk)
            request.reject()
            return Response({"detail":"Request rejected"}, status=status.HTTP_200_OK)
        return Response({"detail":"Request ID not provided"}, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def remove_friend(self, request, pk=None):
        """
            `user_id`: User id of the user who has to removed
        """
        pk = request.data.get('user_id')
        if pk:
            self.object = request.user
            other_user = User.objects.get(pk=pk)
            Friend.objects.remove_friend(self.object, other_user)
            return Response({"friends":"Friend removed"}, status=status.HTTP_200_OK)
        return Response({"detail":"User ID not provided"}, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def are_friends(self, request, pk=None):
        """
            Check if the logged in user and the other user are friends

            `user_id`: User id of the user who has to be checked
        """
        pk = request.data.get('user_id')
        if pk:
            self.object = request.user
            other_user = User.objects.get(pk=pk)
            value = Friend.objects.are_friends(self.object, other_user)
            print(value)
            return Response({"detail":value}, status=status.HTTP_200_OK)
        return Response({"detail":"User ID not provided"}, status=status.HTTP_400_BAD_REQUEST)