from allauth.utils import generate_unique_username
from django.contrib.auth import get_user_model
from rest_framework import serializers

from friendship.models import Friend, Follow, Block, FriendshipRequest

User = get_user_model()

class SignupSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'username', 'password')
        extra_kwargs = {
            'password': {
                'write_only': True,
                'style': {
                    'input_type': 'password'
                }
            },
            'email': {
                'required': True
            }
        }

    def create(self, validated_data):
        user = User(
            email=validated_data.get('email'),
            first_name=validated_data.get('first_name'),
            last_name=validated_data.get('last_name'),
            username=validated_data.get('username'),
        )
        user.set_password(validated_data.get('password'))
        user.save()
        return user

    def validate_email(self, value):
        if not value:
            raise serializers.ValidationError('This field is required.')
        return value


class UserSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = User
        exclude = ['groups', 'password', 'user_permissions']


class UserProfileSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = User
        exclude = [
            'groups', 
            'password', 
            'user_permissions', 
            'is_active', 
            'last_login',
            'is_superuser',
            'is_staff',
            'email',
            ]
        read_only_fields = ['email', 'username', 'date_joined']


class FriendshipRequestSerializer(serializers.ModelSerializer):
    to_user = UserSerializer(read_only=True)
    from_user = UserSerializer(read_only=True)
    class Meta:
        model = FriendshipRequest
        fields = '__all__'