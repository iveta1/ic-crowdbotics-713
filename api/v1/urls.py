from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from api.v1.viewsets import SignupViewSet, LoginViewSet, FriendshipViewSet, UserProfileViewSet
from channel.viewsets import ChannelViewSet, GrabsViewSet, CommentViewSet, PostViewSet
from chat.viewsets import ChatRoomViewSet
from video.viewsets import VideoViewSet

router = DefaultRouter()
router.register('signup', SignupViewSet, base_name='signup')
router.register('login', LoginViewSet, base_name='login')
router.register('profile', UserProfileViewSet, base_name='profile')
router.register('friendship', FriendshipViewSet, base_name='friendship')

router.register('channels', ChannelViewSet, base_name='channels')
router.register('videos', VideoViewSet, base_name='videos')

router.register('grabs', GrabsViewSet, base_name='grabs')
router.register('posts', PostViewSet, base_name='posts')
router.register('comments', CommentViewSet, base_name='comments')

router.register('chat-rooms', ChatRoomViewSet, base_name='chat_rooms')

urlpatterns = [
    url(r'', include(router.urls)),
]
