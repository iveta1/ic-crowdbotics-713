from rest_framework import permissions

class IsGETOrIsAuthenticated(permissions.BasePermission):        

    def has_permission(self, request, view):
        # allow all GET requests
        if request.method == 'GET':
            return True

        # Otherwise, only allow authenticated requests
        return request.user and request.user.is_authenticated


class HasObjectOrIsStaff(permissions.BasePermission):        

    def has_object_permission(self, request, view, obj):
        # Allow get requests for all
        if request.method == 'GET':
            return True
        return (request.user == obj.owner) or request.user.is_staff


class IsOwnerOrPublisher(permissions.BasePermission):        

    def has_object_permission(self, request, view, obj):
        # Allow get requests for all
        if request.method == 'GET':
            return True
        return (request.user == obj.owner) or (request.user in obj.publishers)


class IsCurrentUserOrIsStaff(permissions.BasePermission):        

    def has_object_permission(self, request, view, obj):
        # Allow get requests for all
        if request.method == 'GET':
            return True
        return (request.user == obj) or request.user.is_staff