from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from django.views.generic import UpdateView
from django.urls import reverse_lazy
from django.contrib import messages
from django.http import HttpResponse

from .forms import UserProfileForm

# Create your views here.
User = get_user_model()

def home(request):
    packages = [
	{'name':'djangorestframework', 'url': 'http://pypi.python.org/pypi/djangorestframework/3.7.7'},
	{'name':'django-bootstrap4', 'url': 'http://pypi.python.org/pypi/django-bootstrap4/0.0.5'},
	{'name':'django-allauth', 'url': 'http://pypi.python.org/pypi/django-allauth/0.34.0'},
    ]
    context = {
        'packages': packages
    }
    return render(request, 'home/index.html', context)


class UserProfileUpdateView(UpdateView):
    model = User
    template_name = "account/profile.html"
    form_class = UserProfileForm
    slug_field = 'username'
    slug_url_kwarg = 'username'

    def get_success_url(self, *args, **kwargs):
        return reverse_lazy('profile', kwargs={'username': self.object.username})


def submit_for_verification(request):
    user = request.user
    done = False

    if user.is_authenticated():
        done, msg = user.submit_for_verification()

    if not done:
        messages.warning(request, msg)
    else:
        messages.success(request, msg)

    return redirect(reverse_lazy('profile', kwargs={'username': user.username}))
