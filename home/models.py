from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver

class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

class User(AbstractUser):
    picture = models.ImageField("Profile Picture", upload_to='profilepics/%Y/%m/%d/', null=True, blank=True)

    for_verification = models.BooleanField(default=False)
    verified = models.BooleanField(default=False)
    public = models.BooleanField(default=True)

    def submit_for_verification(self):
        if not self.public:
            return False, "Profile not set to public"
        self.for_verification = True
        self.save()
        return True, "Successful"

    def is_channel_unique(self, name=None):
        return not Channel.objects.filter(name=name if name else self.username).exists()

    def generate_name(self):
        for i in range(1,100):
            name = f'{self.username}({i})'
            if self.is_channel_unique(name):
                return name
        return None

    def create_channel(self):
        if self.is_channel_unique():
            Channel.objects.create(
                    owner=self,
                    name=self.username
                )
        else:
            name = self.generate_name()
            if name:
                Channel.objects.create(
                        owner=self,
                        name=name
                    )
            # if name not found then what?


    def verify(self):
        self.for_verification = False
        self.verified = True

    def save(self, *args, **kwargs):
        if self.verified:
            self.verify()
        super(User, self).save(*args, **kwargs)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        instance.create_channel()


from channel.models import Channel