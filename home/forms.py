from django import forms
from django.contrib.auth import get_user_model

# Create your views here.
User = get_user_model()
class UserProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('picture', 'first_name', 'last_name', 'public')
