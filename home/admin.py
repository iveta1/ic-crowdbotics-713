from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
# Register your models here.

User = get_user_model()
class UserAdmin(DjangoUserAdmin):
    fieldsets = (
		(None, {'fields': ('username', 'email', 'password')}),
		(('Personal info'), {'fields': ('first_name', 'last_name')}),
		(('Booleans'), {'fields': ('for_verification', 'verified', 'public')}),
		(('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
									   'groups', 'user_permissions')}),
		(('Important dates'), {'fields': ('last_login', 'date_joined')}),
	)
    list_display = ['email', 'username',]
    list_filter = ['for_verification',]

admin.site.register(User, UserAdmin)