
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home, name="home"),
    url(r'^accounts/profile/(?P<username>\w+)/$', views.UserProfileUpdateView.as_view(), name="profile"),
    url(r'^submit_for_verification/$', views.submit_for_verification, name="submit_for_verification"),
]
