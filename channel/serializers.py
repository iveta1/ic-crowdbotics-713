from rest_framework import serializers

from .models import Channel, Grabs, Comment, Post
from api.v1.serializers import UserSerializer


class ChannelSerializer(serializers.ModelSerializer):
	owner = UserSerializer(read_only=True)
	class Meta:
		model = Channel
		fields = '__all__'
		read_only_fields = ('owner', 'verified', 'public', 'for_verification', 'active')


class GrabsSerializer(serializers.ModelSerializer):
	owner = UserSerializer(read_only=True)
	class Meta:
		model = Grabs
		fields = '__all__'
		read_only_fields = ('owner', 'verified', 'public', 'for_verification', 'active', 'monetized')


class CommentSerializer(serializers.ModelSerializer):
	object_id = serializers.CharField()
	content_type = serializers.CharField()

	class Meta:
		model = Comment
		fields = '__all__'
		read_only_fields = ('user', 'created', 'modified', 'deleted', 'blocked')


class CommentGetSerializer(serializers.ModelSerializer):
	comments = CommentSerializer(many=True, read_only=True)
	user = UserSerializer(read_only=True)
	class Meta:
		model = Comment
		fields = '__all__'


class PostSerializer(serializers.ModelSerializer):
	comments = CommentSerializer(many=True, read_only=True)
	class Meta:
		model = Post
		fields = '__all__'
		read_only_fields = ('user', 'comments')


class PostGetSerializer(serializers.ModelSerializer):
	comments = CommentSerializer(many=True, read_only=True)
	user = UserSerializer(read_only=True)
	class Meta:
		model = Post
		fields = '__all__'
		