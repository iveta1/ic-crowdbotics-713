from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.viewsets import ModelViewSet, ViewSet
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from django.contrib.contenttypes.models import ContentType

from .models import Channel, Grabs, Comment, Post
from .serializers import (
    ChannelSerializer, 
    GrabsSerializer, 
    CommentSerializer, 
    PostSerializer,
    PostGetSerializer,
    CommentGetSerializer
    )

from api.v1.permissions import IsGETOrIsAuthenticated, HasObjectOrIsStaff, IsOwnerOrPublisher

class ChannelViewSet(ModelViewSet):
    """
    ViewSet for Channel
    """
    model = Channel
    queryset = Channel.objects.all()
    serializer_class = ChannelSerializer
    permission_classes = (IsGETOrIsAuthenticated, HasObjectOrIsStaff)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    @action(detail=True, methods=['post'], permission_classes=[IsAuthenticated, HasObjectOrIsStaff])
    def submit_for_verification(self, request, pk=None):
        self.object = self.model.objects.get(pk=pk)
        done, msg = self.object.submit_for_verification()
        return Response({'detail':msg}, status=status.HTTP_200_OK if done else status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post'], permission_classes=[IsAuthenticated, IsOwnerOrPublisher])
    def add_moderators(self, request, pk=None):
        """
            `user_id`: Array of user IDs
        """
        self.object = self.model.objects.get(pk=pk)
        user_pk_list = request.data.get('user_id')
        if user_pk_list:
            for pk in user_pk_list:
                user  = User.objects.filter(pk=pk).first()
                self.object.moderators.add(user)
            return Response({'detail':"Moderator(s) added."}, status=status.HTTP_200_OK)
        return Response({'detail':"No user list given."}, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post'], permission_classes=[IsAuthenticated, HasObjectOrIsStaff])
    def add_publishers(self, request, pk=None):
        """
            `user_id`: Array of user IDs
        """
        self.object = self.model.objects.get(pk=pk)
        user_pk_list = request.data.get('user_id')
        if user_pk_list:
            for pk in user_pk_list:
                user  = User.objects.filter(pk=pk).first()
                self.object.publishers.add(user)
            return Response({'detail':"Publishers(s) added."}, status=status.HTTP_200_OK)
        return Response({'detail':"No user list given."}, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post'], permission_classes=[IsAuthenticated, HasObjectOrIsStaff])
    def add_editors(self, request, pk=None):
        """
            `user_id`: Array of user IDs
        """
        self.object = self.model.objects.get(pk=pk)
        user_pk_list = request.data.get('user_id')
        if user_pk_list:
            for pk in user_pk_list:
                user  = User.objects.filter(pk=pk).first()
                self.object.editors.add(user)
            return Response({'detail':"Editor(s) added."}, status=status.HTTP_200_OK)
        return Response({'detail':"No user list given."}, status=status.HTTP_400_BAD_REQUEST)


class GrabsViewSet(ModelViewSet):
    """
    ViewSet for Grabs
    """
    model = Grabs
    queryset = Grabs.objects.all()
    serializer_class = GrabsSerializer
    permission_classes = (IsGETOrIsAuthenticated, HasObjectOrIsStaff)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    @action(detail=True, methods=['post'], permission_classes=[IsAuthenticated, HasObjectOrIsStaff])
    def submit_for_verification(self, request, pk=None):
        self.object = self.model.objects.get(pk=pk)
        done, msg = self.object.submit_for_verification()
        return Response({'detail':msg}, status=status.HTTP_200_OK if done else status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post'], permission_classes=[IsAuthenticated, HasObjectOrIsStaff])
    def monetize(self, request, pk=None):
        self.object = self.model.objects.get(pk=pk)
        done, msg = self.object.monetize()
        return Response({'detail':msg}, status=status.HTTP_200_OK if done else status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post'], permission_classes=[IsAuthenticated, HasObjectOrIsStaff])
    def add_moderators(self, request, pk=None):
        """
            `user_id`: Array of user IDs
        """
        self.object = self.model.objects.get(pk=pk)
        user_pk_list = request.data.get('user_id')
        if user_pk_list:
            for pk in user_pk_list:
                user  = User.objects.filter(pk=pk).first()
                self.object.moderators.add(user)
            return Response({'detail':"Moderator(s) added."}, status=status.HTTP_200_OK)
        return Response({'detail':"No user list given."}, status=status.HTTP_400_BAD_REQUEST)


class PostViewSet(ModelViewSet):
    """
    ViewSet for Post
    """
    model = Post
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    serializer_class_get = PostGetSerializer
    permission_classes = (IsGETOrIsAuthenticated,)

    def get_serializer_class(self):
        if self.request.method == "GET":
            return self.serializer_class_get
        return self.serializer_class

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CommentViewSet(ModelViewSet):
    """
    ViewSet for Comment
    """
    model = Comment
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    serializer_class_get = CommentGetSerializer
    permission_classes = (IsGETOrIsAuthenticated,)

    def get_serializer_class(self):
        if self.request.method == "GET":
            return self.serializer_class_get
        return self.serializer_class

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        content_type = serializer.validated_data['content_type']
        object_id = serializer.validated_data['object_id']
        text = serializer.validated_data['text']
        anonymous = serializer.validated_data['anonymous']

        content_type = ContentType.objects.get(model=content_type)

        comment = Comment.objects.create(text=text,
                          content_type=content_type,
                          object_id=object_id,
                          user=request.user,
                          anonymous=anonymous)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
