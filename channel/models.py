from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from home.models import BaseModel

User = get_user_model()

class Channel(BaseModel):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="channels")
    name = models.CharField("Name", max_length=150, unique=True)

    moderators = models.ManyToManyField(User, related_name='channel_moderators')
    publishers = models.ManyToManyField(User, related_name='channel_publishers')
    editors = models.ManyToManyField(User, related_name='channel_editors')

    verified = models.BooleanField(default=False)
    for_verification = models.BooleanField(default=False)
    active = models.BooleanField(default=False)

    def submit_for_verification(self):
        # Conditions come here

        self.for_verification = True
        self.save()
        return True, "Successful"

    def is_grabs_unique(self, name=None):
        return not Grabs.objects.filter(name=name if name else self.name).exists()

    def generate_name(self):
        for i in range(1,100):
            name = f'{self.name}({i})'
            if self.is_grabs_unique(name):
                return name
        return None

    def create_grabs(self):
        if self.is_grabs_unique():
            Grabs.objects.create(
                    owner=self.owner,
                    name=self.name
                )
        else:
            name = self.generate_name()
            if name:
                Grabs.objects.create(
                        owner=self.owner,
                        name=name
                    )
            # if name not found then what?

    def verify(self):
        self.for_verification = False
        self.verified = True

    def save(self, *args, **kwargs):
        if self.verified:
            self.verify()
        super(Channel, self).save(*args, **kwargs)

@receiver(post_save, sender=Channel)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        instance.create_grabs()


class Grabs(BaseModel):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="grabs")
    name = models.CharField("Name", max_length=150, unique=True)

    moderators = models.ManyToManyField(User, related_name='grabs_moderators')

    monetized = models.BooleanField(default=False)
    nsfw = models.BooleanField(default=False)

    verified = models.BooleanField(default=False)
    for_verification = models.BooleanField(default=False)
    active = models.BooleanField(default=False)

    def submit_for_verification(self):
        # Conditions come here
        
        self.for_verification = True
        self.save()
        return True, "Successful"

    def verify(self):
        self.for_verification = False
        self.verified = True

    def monetize(self):
        # Monetization condition go here

        self.monetize = True
        self.save()
        return True, "Successful"

    def save(self, *args, **kwargs):
        if self.verified:
            self.verify()
        super(Grabs, self).save(*args, **kwargs)


class Comment(BaseModel):
    user = models.ForeignKey(User, related_name='comments', null=True, blank=True)
    text = models.TextField(blank=True)

    anonymous = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)
    blocked = models.BooleanField(default=False)

    content_type = models.ForeignKey(ContentType)
    object_id = models.CharField(max_length=50)
    content_object = GenericForeignKey('content_type', 'object_id')

    comments = GenericRelation('comment')


class Post(BaseModel):
    user = models.ForeignKey(User, related_name='posts')
    grabs = models.ForeignKey(Grabs, related_name='posts')
    text = models.TextField(blank=True)

    comments = GenericRelation('comment')