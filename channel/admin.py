from django.contrib import admin

from .models import Channel, Grabs, Comment, Post
# Register your models here.

class ChannelAdmin(admin.ModelAdmin):
    '''
        Admin View for Channel
    '''
    list_display = ('name', 'verified')
    list_filter = ('for_verification',)

admin.site.register(Channel, ChannelAdmin)

class GrabsAdmin(admin.ModelAdmin):
    '''
        Admin View for Grabs
    '''
    list_display = ('name', 'verified')
    list_filter = ('for_verification',)

admin.site.register(Grabs, GrabsAdmin)

class CommentAdmin(admin.ModelAdmin):
    '''
        Admin View for Comment
    '''
    pass

admin.site.register(Comment, CommentAdmin)

class PostAdmin(admin.ModelAdmin):
    '''
        Admin View for Post
    '''
    pass

admin.site.register(Post, PostAdmin)