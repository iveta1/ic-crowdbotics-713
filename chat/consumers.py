import json

from channels.auth import get_user
from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from chat.models import ChatRoom, ChatMessage
from chat.serializers import ChatMessageSerializer


class TestChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.user = self.scope["user"]
        self.room_name = self.scope['url_route']['kwargs']['chat_id']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data=None, bytes_data=None):
        # await database_sync_to_async(self.scope["session"].save)()
        # user = await get_user(self.scope)
        # if not user.is_authenticated():
        #     await self.close()
        #     return

        text_data_json = json.loads(text_data)
        message = text_data_json['content']

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.user = self.scope["user"]
        self.room_name = self.scope['url_route']['kwargs']['chat_id']
        self.chat_room = ChatRoom.objects.get(pk=self.room_name)
        self.room_group_name = 'chat_%s' % self.room_name

        if not self.chat_room.participants.filter(pk=self.user.pk).exists():
            await self.close()
            return

        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data=None, bytes_data=None):
        # await database_sync_to_async(self.scope["session"].save)()
        user = await get_user(self.scope)
        if not user.is_authenticated():
            await self.close()
            return

        text_data_json = json.loads(text_data)
        message = text_data_json['content'].strip()
        if message:
            chat_message = await database_sync_to_async(ChatMessage.objects.create)(
                sender=user,
                room=self.chat_room,
                content=message
            )
            message_serializer = ChatMessageSerializer(instance=chat_message, context={'user': user})
            # Send message to room group
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'message': message_serializer.data
                }
            )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))
