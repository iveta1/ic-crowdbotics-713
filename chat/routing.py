from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/testchat/(?P<chat_id>[^/]+)/$', consumers.TestChatConsumer),  # TODO remove before going live
    url(r'^ws/chat/(?P<chat_id>[^/]+)/$', consumers.ChatConsumer),
]
