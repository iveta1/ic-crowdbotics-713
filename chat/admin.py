from django.contrib import admin

from chat.models import ChatRoom, ChatMessage


class ChatRoomAdmin(admin.ModelAdmin):
    list_display = ('id', 'is_dm',)


admin.site.register(ChatRoom, ChatRoomAdmin)


class ChatMessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'sender', 'room', 'content', 'sent_at',)


admin.site.register(ChatMessage, ChatMessageAdmin)
