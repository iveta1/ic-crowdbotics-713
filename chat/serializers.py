from django.db.models import Count
from rest_framework import serializers

from chat.models import ChatRoom, ChatMessage


class ChatRoomSerializer(serializers.ModelSerializer):
    room_name = serializers.SerializerMethodField(read_only=True)

    def get_room_name(self, obj):
        participants = obj.participants.exclude(pk=self.context['request'].user.pk)
        other_participants_count = participants.count()

        if obj.is_dm:
            return '{}'.format(participants.first())
        elif other_participants_count == 1:
            return 'You & {}'.format(participants.first())
        else:
            return 'You & {} others'.format(participants.count())

    def validate(self, attrs):
        # TODO check non-friends validation
        participants_count = len(attrs['participants'])
        if attrs['is_dm'] and participants_count != 2:
            raise serializers.ValidationError('Invalid request')

        if not attrs['is_dm'] and participants_count < 2:
            raise serializers.ValidationError('Group must have at least one member.')

        if participants_count > 20:
            raise serializers.ValidationError('Group cannot have more than 20 members.')

        return attrs

    def create(self, validated_data):
        if validated_data['is_dm']:
            chat_room = ChatRoom.objects \
                .filter(participants__in=validated_data['participants'], is_dm=validated_data['is_dm']) \
                .annotate(num_participants=Count('participants')) \
                .filter(num_participants=len(validated_data['participants']))
            if chat_room.exists():
                return chat_room.first()

        return super().create(validated_data)

    class Meta:
        model = ChatRoom
        fields = ('id', 'participants', 'is_dm', 'room_name',)
        extra_kwargs = {
            'participants': {
                'write_only': True,
            },
            'is_dm': {
                'write_only': True,
            },
        }


class ChatMessageSerializer(serializers.ModelSerializer):
    sender_name = serializers.CharField(source='sender.first_name', read_only=True)
    sender_uname = serializers.CharField(source='sender.username', read_only=True)
    by_self = serializers.SerializerMethodField(read_only=True)

    def get_by_self(self, obj):
        return obj.sender == self.context['user']

    class Meta:
        model = ChatMessage
        fields = ('id', 'sender', 'content', 'sent_at', 'sender_name', 'sender_uname', 'by_self',)
        extra_kwargs = {
            'sender': {
                'write_only': True,
            },
        }
