from django.db import models

from home.models import User


class ChatRoom(models.Model):
    id = models.BigAutoField(primary_key=True, unique=True)
    participants = models.ManyToManyField(User, related_name='chat_rooms')
    is_dm = models.BooleanField(default=False)

    def __str__(self):
        return "Room #{}".format(self.pk)


class ChatMessage(models.Model):
    id = models.BigAutoField(primary_key=True, unique=True)
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='chat_messages')
    room = models.ForeignKey(ChatRoom, on_delete=models.CASCADE, related_name='chat_messages')
    content = models.TextField()
    sent_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.content[:40] + "..."
