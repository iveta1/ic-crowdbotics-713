import datetime

from rest_framework import viewsets, pagination, mixins, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from chat.models import ChatRoom, ChatMessage
from chat.serializers import ChatRoomSerializer, ChatMessageSerializer


class ChatRoomViewSet(mixins.CreateModelMixin,
                      mixins.ListModelMixin,
                      viewsets.GenericViewSet):
    serializer_class = ChatRoomSerializer
    pagination_class = pagination.PageNumberPagination
    pagination_class.page_size = 10
    permission_classes = [IsAuthenticated,]

    def get_queryset(self):
        return ChatRoom.objects.filter(participants__in=[self.request.user])

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['user'] = self.request.user
        return context

    @action(detail=True, methods=['get'], permission_classes=[IsAuthenticated], serializer_class=ChatMessageSerializer)
    def get_messages(self, request, pk=None):
        """
            `timestamp`: Upto 10 messages older than this will be retrieved. Default: Current Timestamp
        """
        chat_room = ChatRoom.objects.get(pk=pk)
        timestamp = request.GET.get('timestamp', datetime.datetime.now())
        messages = ChatMessage.objects.filter(room=chat_room, sent_at__lt=timestamp).order_by('-sent_at')[:10]
        serializer = ChatMessageSerializer(instance=messages, many=True, context=self.get_serializer_context())
        return Response(serializer.data, status=status.HTTP_201_CREATED)
