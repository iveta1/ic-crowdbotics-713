"""
ASGI entrypoint. Configures Django and then runs the application
defined in the ASGI_APPLICATION setting.
# daphne -p 8001 ic_crowdbotics_713.asgi:application
"""

import os

import django
from channels.routing import get_default_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ic_crowdbotics_713.settings")
django.setup()
application = get_default_application()
